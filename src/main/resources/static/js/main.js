$(document).ready(function () {
    $("button.country").on("click", function() {
        event.preventDefault();

        get_countries();
    });

    $('div').on("click", "a.country.dropdown-item", function() {
        event.preventDefault();

        var city_button = '<button type="button" class="btn btn-primary dropdown-toggle city" data-toggle="dropdown" id='+ $(this).text() +'>\n' +
            'City\n' +
            '</button><div class="city dropdown-menu">\n' +
            '</div>';

        $("div.dropdown.city").empty();
        $("div.dropdown.city").append(city_button);
        generate_cinemas($(this).text());
    });

    $("div").on("click", "button.city", function() {
        event.preventDefault();

        get_cities($(this).attr('id'));
    });

    $('div').on("click", "a.city.dropdown-item", function() {
        event.preventDefault();

        generate_cinemas($(this).attr('id'), $(this).text());
    });

    function get_countries() {
        $.ajax({
            type: "GET",
            contentType: "application/json",
            url: "/countries",
            dataType: 'json',
            cache: false,
            success: function (data) {
                var html = '';

                for (var row in data) {
                    console.log(data[row]);
                    html += '<a class="country dropdown-item" href="#">';
                    html += data[row];
                    html += '</a>';
                }

                $("div.country.dropdown-menu").empty();
                $("div.country.dropdown-menu").append(html);
            },
            error: function (e) {
                console.log(e);
            }
        });
    }

    function get_cities(country) {
        $.ajax({
            type: "GET",
            contentType: "application/json",
            url: "/cities?country=" + country,
            dataType: 'json',
            cache: false,
            success: function (data) {
                var html = '';

                console.log(data);

                for (var row in data) {
                    console.log(data[row]);
                    html += '<a class="city dropdown-item" href="#" id='+ country +'>';
                    html += data[row];
                    html += '</a>';
                }

                $("div.city.dropdown-menu").empty();
                $("div.city.dropdown-menu").append(html);
            },
            error: function (e) {
                console.log(e);
            }
        });
    }

    function generate_cinemas(country, city) {
        var url = "/cinemas?country=" + country;
        if (city !== undefined) {
            url += "&city=" + city;
        }
        $.ajax({
            type: "GET",
            contentType: "application/json",
            url: url,
            dataType: 'json',
            cache: false,
            success: function (data) {
                var html = '';

                html += '<h1>Cinemas</h1><table class="table">\n' +
                    '<thead class="thead-dark">\n' +
                    '<tr>\n' +
                    '<th scope="col">Name</th>\n' +
                    '<th scope="col">Rating</th>\n' +
                    '</tr>\n' +
                    '</thead>\n' +
                    '<tbody>';

                for (var row in data) {
                    html += '<tr>';
                    html += '<td><a href="/'+ data[row]['id'] +'/movies">' + data[row]['name'] +'</a></td>';
                    html += '<td>' + data[row]['rating'] +'</td>';
                    html += '</tr>';
                }

                html += '</tbody>';
                html += '</table>';

                $("div.panel-group.cinemas").empty();
                $("div.panel-group.cinemas").append(html);
            },
            error: function (e) {
                console.log(e);
            }
        });
    }
});