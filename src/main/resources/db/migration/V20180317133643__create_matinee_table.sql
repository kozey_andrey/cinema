CREATE TABLE matinee (
  id SERIAL PRIMARY KEY,
  seat INT NOT NULL,
  seance_time TIMESTAMP NOT NULL,
  created_at TIMESTAMP NOT NULL
);
