ALTER TABLE matinees
  ADD COLUMN movie_id INT NOT NULL REFERENCES movies(id);
