CREATE TABLE users (
  id         SERIAL PRIMARY KEY,
  first_name VARCHAR(20) NOT NULL,
  last_name  VARCHAR(20) NOT NULL,
  user_name   VARCHAR(20) NOT NULL,
  password   VARCHAR(60) NOT NULL
);

CREATE INDEX ON users (user_name);