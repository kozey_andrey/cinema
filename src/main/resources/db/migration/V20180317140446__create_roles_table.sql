CREATE TABLE roles (
  id   SERIAL PRIMARY KEY,
  role VARCHAR(20) NOT NULL
);