CREATE TABLE users_roles (
  user_id INT NOT NULL REFERENCES users(id),
  role_id INT NOT NULL REFERENCES roles(id),

  PRIMARY KEY (user_id, role_id)
);