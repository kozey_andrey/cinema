package com.geekhub.j4w.matinee;

import com.geekhub.j4w.user.User;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class MatineeDTO {
    private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private Integer id;
    private Integer seat;
    private String seanceTime;
    private String createdAt;
    private String userName;

    public MatineeDTO(Integer id, Integer seat, LocalDateTime seanceTime, LocalDateTime createdAt, User user) {
        this.id = id;
        this.seat = seat;
        this.seanceTime = seanceTime.format(dateFormatter);
        this.createdAt = createdAt.format(dateTimeFormatter);
        this.userName = user.getFirstName() + " " + user.getLastName();
    }

    public Integer getId() {
        return id;
    }

    public Integer getSeat() {
        return seat;
    }

    public String getSeanceTime() {
        return seanceTime;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUserName() {
        return userName;
    }
}
