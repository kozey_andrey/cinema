package com.geekhub.j4w.matinee;

import com.geekhub.j4w.movie.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import javax.validation.Valid;

@Controller
public class MatineeController {
    private final MatineeService matineeService;

    @Autowired
    public MatineeController(MatineeService matineeService) {
        this.matineeService = matineeService;
    }

    @RequestMapping(value = "/admin/matinee/{id}", method = RequestMethod.GET)
    public String show(Model model, @PathVariable(value = "id") Integer id) {
        MatineeDTO matinee = matineeService.findById(id);
        model.addAttribute("matinee", matinee);

        return "admin/matinee/show";
    }

    @RequestMapping(value = "/matinee/{movie_id}/{seat}", method = RequestMethod.POST)
    public String create(@PathVariable(value = "movie_id") Integer movieId,
                         @PathVariable(value = "seat") Integer seat) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        matineeService.save(auth.getName(), movieId, seat);

        return "redirect:/movie/" + movieId;
    }

    @RequestMapping(value = "/admin/matinee/{id}", method = RequestMethod.PUT)
    public String update(Model model, @Valid Matinee matinee, BindingResult bindingResult) {
        if (!bindingResult.hasErrors()) {
            matineeService.save(matinee);
            model.addAttribute("successMessage", "Matinee has been updated successfully");
        }

        model.addAttribute("matinee", matinee);

        return "admin/matinee/show";
    }

    @RequestMapping(value = "/admin/matinee/{id}", method = RequestMethod.DELETE)
    public String delete(@PathVariable(value = "id") Integer id) {
        Movie movie = matineeService.delete(id);
        return "redirect:/admin/movie/" + movie.getId();
    }
}
