package com.geekhub.j4w.matinee;

import com.geekhub.j4w.movie.Movie;
import com.geekhub.j4w.movie.MovieService;
import com.geekhub.j4w.user.User;
import com.geekhub.j4w.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MatineeService {
    private final MatineeRepository matineeRepository;
    private final MovieService movieService;
    private final UserService userService;

    @Autowired
    public MatineeService(MatineeRepository matineeRepository, MovieService movieService, UserService userService) {
        this.matineeRepository = matineeRepository;
        this.movieService = movieService;
        this.userService = userService;
    }

    public Matinee findByUserAndMovie(User user, Movie movie) {
        return matineeRepository.findByUserAndMovie(user, movie);
    }

    public MatineeDTO findById(Integer id) {
        return matineeRepository.findByIdDTO(id);
    }

    public Map<Integer, Integer> getIdsAndSeatsByMovie(Movie movie) {
        List<Matinee> matineeList = matineeRepository.findAllByMovie(movie);
        return matineeList.stream().collect(Collectors.toMap(Matinee::getSeat, Matinee::getId));
    }

    public Matinee save(Matinee matinee) {
        return matineeRepository.save(matinee);
    }

    public Matinee save(String userName, Integer movieId, Integer seat) {
        Matinee matinee = new Matinee();
        Movie movie = movieService.findById(movieId);
        User user = userService.findUserByUserName(userName);
        matinee.setUser(user);
        matinee.setMovie(movie);
        matinee.setSeat(seat);
        matinee.setSeanceTime(movie.getRelease().atStartOfDay());

        return matineeRepository.save(matinee);
    }

    public Movie delete(Integer id) {
        Optional<Matinee> matinee = matineeRepository.findById(id);
        if (matinee.isPresent()) {
            matineeRepository.delete(matinee.get());
            return matinee.get().getMovie();
        }

        return null;
    }
}
