package com.geekhub.j4w.matinee;

import com.geekhub.j4w.movie.Movie;
import com.geekhub.j4w.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Repository
public interface MatineeRepository extends JpaRepository<Matinee, Integer> {
    @Query("select m from Matinee m where m.user = ?1 and m.movie = ?2")
    @Transactional(readOnly = true)
    Matinee findByUserAndMovie(User user, Movie movie);

    @Query("select new com.geekhub.j4w.matinee.MatineeDTO(m.id, m.seat, m.seanceTime, m.createdAt, m.user) from Matinee m where m.id = ?1")
    @Transactional(readOnly = true)
    MatineeDTO findByIdDTO(Integer id);

    @Query("select m from Matinee m where m.movie = ?1")
    @Transactional(readOnly = true)
    List<Matinee> findAllByMovie(Movie movie);
}
