package com.geekhub.j4w.matinee;

import com.geekhub.j4w.converter.LocalDateTimeConverter;
import com.geekhub.j4w.movie.Movie;
import com.geekhub.j4w.user.User;
import org.hibernate.annotations.CreationTimestamp;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Table(name = "matinees")
public class Matinee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "seat")
    @NotNull(message = "*Please provide seat")
    private Integer seat;

    @Column(name = "seance_time")
    @Convert(converter = LocalDateTimeConverter.class)
    @NotNull(message = "*Please provide seance time")
    private LocalDateTime seanceTime;

    @Column(name = "created_at", updatable = false, nullable = false)
    @Convert(converter = LocalDateTimeConverter.class)
    @CreationTimestamp
    private LocalDateTime createdAt;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false, updatable = false)
    @NotNull(message = "*Please provide user")
    private User user;

    @ManyToOne
    @JoinColumn(name = "movie_id", nullable = false, updatable = false)
    @NotNull(message = "*Please provide movie")
    private Movie movie;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSeat() {
        return seat;
    }

    public void setSeat(Integer seat) {
        this.seat = seat;
    }

    public LocalDateTime getSeanceTime() {
        return seanceTime;
    }

    public void setSeanceTime(LocalDateTime seanceTime) {
        this.seanceTime = seanceTime;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }
}
