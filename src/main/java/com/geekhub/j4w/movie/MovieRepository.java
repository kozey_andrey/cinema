package com.geekhub.j4w.movie;

import com.geekhub.j4w.cinema.Cinema;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Integer> {
    @Query("select new com.geekhub.j4w.movie.MovieDTO(m.id, m.name, m.description, m.rating, m.release, m.createdAt, count(ma.id)) from Movie m left join m.matinees ma where m.cinema = ?1 group by m.id")
    @Transactional(readOnly = true)
    Page<MovieDTO> findAllByCinema(Cinema cinema, Pageable pageable);

    @Query("select new com.geekhub.j4w.movie.MovieDTO(m.id, m.name, m.description, m.rating, m.release, m.createdAt, count(ma.id)) from Movie m left join m.matinees ma where m.cinema = ?1 group by m.id")
    @Transactional(readOnly = true)
    List<MovieDTO> findAllByCinema(Cinema cinema);
}
