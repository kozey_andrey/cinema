package com.geekhub.j4w.movie;

import com.geekhub.j4w.matinee.MatineeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import javax.validation.Valid;

@Controller
public class MovieController {
    private final MovieService movieService;
    private final MatineeService matineeService;

    @Autowired
    public MovieController(MovieService movieService, MatineeService matineeService) {
        this.movieService = movieService;
        this.matineeService = matineeService;
    }

    @RequestMapping(value = "/{cinema_id}/movies", method = RequestMethod.GET)
    public String index(Model model, @PathVariable(value = "cinema_id") Integer id,
                        @RequestParam(value = "page", defaultValue = "0") Integer page,
                        @RequestParam(value = "limit", defaultValue = "10") Integer limit) {
        Page<MovieDTO> movies = movieService.findAllByCinema(id, page, limit);
        model.addAttribute("movies", movies);
        model.addAttribute("totalPages", movies.getTotalPages());

        return "movie/index";
    }

    @RequestMapping(value = "/movie/{id}", method = RequestMethod.GET)
    public String showToUser(Model model, @PathVariable(value = "id") Integer id) {
        Movie movie = movieService.findById(id);
        model.addAttribute("movie", movie);
        model.addAttribute("matinee_seats", matineeService.getIdsAndSeatsByMovie(movie));
        return "movie/show";
    }

    @RequestMapping(value = "/admin/movie/{id}", method = RequestMethod.GET)
    public String show(Model model, @PathVariable(value = "id") Integer id) {
        Movie movie = movieService.findById(id);
        model.addAttribute("movie", movie);
        model.addAttribute("matinee_seats", matineeService.getIdsAndSeatsByMovie(movie));

        return "admin/movie/show";
    }

    @RequestMapping(value = "/admin/{cinema_id}/movie/new", method = RequestMethod.GET)
    public String create(Model model, @PathVariable(value = "cinema_id") Integer cinemaId) {
        model.addAttribute("cinema_id", cinemaId);
        model.addAttribute("movie", new Movie());

        return "admin/movie/add";
    }

    @RequestMapping(value = "/admin/{cinema_id}/movie/new", method = RequestMethod.POST)
    public String create(Model model, @Valid Movie movie,
                         @PathVariable(value = "cinema_id") Integer cinemaId,
                         BindingResult bindingResult) {
        if (!bindingResult.hasErrors()) {
            movieService.save(movie);
            model.addAttribute("successMessage", "Movie has been added successfully");
            return "redirect:/admin/cinema/" + cinemaId;
        } else {
            model.addAttribute("cinema_id", cinemaId);
            return "admin/movie/add";
        }
    }

    @RequestMapping(value = "/admin/movie/{id}", method = RequestMethod.PUT)
    public String update(Model model, @Valid Movie movie, BindingResult bindingResult) {
        if (!bindingResult.hasErrors()) {
            movieService.save(movie);
            model.addAttribute("successMessage", "Movie has been updated successfully");
            return "redirect:/admin/movie/" + movie.getId();
        } else {
            model.addAttribute("matinee_seats", matineeService.getIdsAndSeatsByMovie(movie));
            return "admin/movie/show";
        }
    }

    @RequestMapping(value = "/admin/movie/{id}", method = RequestMethod.DELETE)
    public String delete(@PathVariable(value = "id") Integer movieId) {
        Integer cinemaId = movieService.delete(movieId);
        return "redirect:/admin/cinema/" + cinemaId;
    }
}
