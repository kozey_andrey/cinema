package com.geekhub.j4w.movie;

import com.geekhub.j4w.cinema.Cinema;
import com.geekhub.j4w.converter.LocalDateTimeConverter;
import com.geekhub.j4w.matinee.Matinee;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Table(name = "movies")
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    @Length(min = 5, max = 20, message = "*Movie name must have at least 5 characters and be less than 20 characters")
    @NotEmpty(message = "*Please provide movie name")
    private String name;

    @Column(name = "description")
    @Length(min = 20, max = 255, message = "*Movie description must have at least 20 characters")
    @NotEmpty(message = "*Please provide movie description")
    private String description;

    @Column(name = "rating")
    @NotNull(message = "*Please provide movie rating")
    private Integer rating;

    @Column(name = "release")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull(message = "*Please provide movie release")
    private LocalDate release;

    @Column(name = "created_at", updatable = false, nullable = false)
    @Convert(converter = LocalDateTimeConverter.class)
    @CreationTimestamp
    private LocalDateTime createdAt;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "cinema_id", nullable = false)
    private Cinema cinema;

    @OneToMany(mappedBy = "movie", cascade = CascadeType.ALL)
    private Set<Matinee> matinees;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public LocalDate getRelease() {
        return release;
    }

    public void setRelease(LocalDate release) {
        this.release = release;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public Cinema getCinema() {
        return cinema;
    }

    public void setCinema(Cinema cinema) {
        this.cinema = cinema;
    }

    public Set<Matinee> getMatinees() {
        return matinees;
    }

    public void setMatinees(Set<Matinee> matinees) {
        this.matinees = matinees;
    }
}
