package com.geekhub.j4w.movie;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class MovieDTO {
    private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private Integer id;
    private String name;
    private String description;
    private Integer rating;
    private String release;
    private String createdAt;
    private Long matineesCount;

    public MovieDTO(Integer id, String name, String description, Integer rating, LocalDate release, LocalDateTime createdAt, Long matineesCount) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.rating = rating;
        this.release = release.format(dateFormatter);
        this.createdAt = createdAt.format(dateTimeFormatter);
        this.matineesCount = matineesCount;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Integer getRating() {
        return rating;
    }

    public String getRelease() {
        return release;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public Long getMatineesCount() {
        return matineesCount;
    }
}
