package com.geekhub.j4w.movie;

import com.geekhub.j4w.cinema.Cinema;
import com.geekhub.j4w.cinema.CinemaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MovieService {
    private final MovieRepository movieRepository;
    private final CinemaService cinemaService;

    @Autowired
    public MovieService(MovieRepository movieRepository, CinemaService cinemaService) {
        this.movieRepository = movieRepository;
        this.cinemaService = cinemaService;
    }

    public List<MovieDTO> findAllByCinema(Integer cinemaId) {
        Cinema cinema = cinemaService.findById(cinemaId);
        return movieRepository.findAllByCinema(cinema);
    }

    public Page<MovieDTO> findAllByCinema(Integer cinemaId, Integer page, Integer size) {
        Cinema cinema = cinemaService.findById(cinemaId);
        return movieRepository.findAllByCinema(cinema, PageRequest.of(page, size));
    }

    public Movie findById(Integer id) {
        Optional<Movie> movie = movieRepository.findById(id);
        if (movie.isPresent()) {
            return movie.get();
        }

        return null;
    }

    public Movie save(Movie movie) {
        return movieRepository.save(movie);
    }

    public Integer delete(Integer movieId) {
        Movie movie = findById(movieId);
        movieRepository.delete(movie);

        return movie.getCinema().getId();
    }
}
