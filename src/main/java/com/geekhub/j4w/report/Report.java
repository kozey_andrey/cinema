package com.geekhub.j4w.report;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.time.LocalDateTime;

public abstract class Report {
    public void generate(String className, String format, HttpServletResponse response) throws IOException, DocumentException {
        String fileName = className + "_report_" + LocalDateTime.now() + "." + format;

        try (OutputStream outputStream = new FileOutputStream(fileName)) {
            if (format.equals("xls")) {
                Workbook document = new HSSFWorkbook();
                xls(document);
                document.write(outputStream);
            } else if (format.equals("pdf")) {
                Document document = new Document(PageSize.A4.rotate());
                PdfWriter.getInstance(document, outputStream);

                document.open();
                document.add(pdf());
                document.close();
            } else if (format.equals("doc")) {
                XWPFDocument document = new XWPFDocument();
                doc(document);
                document.write(outputStream);
            }
        }

        try (InputStream is = new FileInputStream(fileName)) {
            IOUtils.copy(is, response.getOutputStream());
        }

        headers(format, fileName, response);
        response.flushBuffer();
    }

    public abstract void xls(Workbook workbook);

    public abstract PdfPTable pdf();

    public abstract XWPFTable doc(XWPFDocument document);

    private void headers(String format, String fileName, HttpServletResponse response) {
        response.setContentType("application/" + format);
        response.setHeader("Content-disposition", "attachment; filename=\"" + fileName + "\"");
    }
}
