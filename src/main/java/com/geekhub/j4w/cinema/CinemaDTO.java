package com.geekhub.j4w.cinema;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class CinemaDTO {
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private Integer id;
    private String name;
    private String createdAt;
    private String country;
    private String city;
    private Integer rating;

    public CinemaDTO(Integer id, String name, LocalDateTime createdAt, String country, String city, Integer rating) {
        this.id = id;
        this.name = name;
        this.createdAt = createdAt.format(formatter);
        this.country = country;
        this.city = city;
        this.rating = rating;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }

    public Integer getRating() {
        return rating;
    }
}
