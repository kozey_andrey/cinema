package com.geekhub.j4w.cinema;

import com.geekhub.j4w.movie.MovieDTO;
import com.geekhub.j4w.movie.MovieService;
import com.itextpdf.text.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@Controller
public class CinemaController {
    private final CinemaService cinemaService;
    private final MovieService movieService;
    private final CinemaReport cinemaReport;

    @Autowired
    public CinemaController(CinemaService cinemaService, MovieService movieService, CinemaReport cinemaReport) {
        this.cinemaService = cinemaService;
        this.movieService = movieService;
        this.cinemaReport = cinemaReport;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index() {
        return "cinema/index";
    }

    @RequestMapping(value = "/cinemas", method = RequestMethod.GET)
    @ResponseBody
    public List<CinemaDTO> all(
            @RequestParam(value = "country", required = false) String country,
            @RequestParam(value = "city", required = false) String city) {
        List<CinemaDTO> cinemas;
        if (country != null) {
            if (city != null) {
                cinemas = cinemaService.filteredByCountryAndCity(country, city);
            } else {
                cinemas = cinemaService.filteredByCountry(country);
            }
        } else {
            cinemas = cinemaService.findAll();
        }

        return cinemas;
    }

    @RequestMapping(value = "/cinema/{id}", method = RequestMethod.GET)
    public String show(Model model, @PathVariable(value = "id") Integer cinemaId) {
        Cinema cinema = cinemaService.findById(cinemaId);
        model.addAttribute("cinema", cinema);

        return "cinema/show";
    }

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String indexAdmin(Model model,
                             @RequestParam(value = "page", defaultValue = "0") Integer page,
                             @RequestParam(value = "limit", defaultValue = "10") Integer limit) {
        Page<CinemaDTO> cinemaList = cinemaService.findAll(page, limit);
        model.addAttribute("cinemas", cinemaList);
        model.addAttribute("totalPages", cinemaList.getTotalPages());

        return "admin/cinema/index";
    }

    @RequestMapping(value = "/admin/cinema/{id}", method = RequestMethod.GET)
    public String create(Model model, @PathVariable(value = "id") Integer id) {
        model.addAttribute("cinema", cinemaService.findById(id));
        model.addAttribute("movies", movieService.findAllByCinema(id));

        return "admin/cinema/show";
    }

    @RequestMapping(value = "/admin/cinema/new", method = RequestMethod.GET)
    public String create(Model model) {
        model.addAttribute("cinema", new Cinema());

        return "admin/cinema/add";
    }

    @RequestMapping(value = "/admin/cinema/new", method = RequestMethod.POST)
    public String create(Model model, @Valid Cinema cinema, BindingResult bindingResult) {
        if (!bindingResult.hasErrors()) {
            cinemaService.save(cinema);
            model.addAttribute("successMessage", "Cinema has been created successfully");

            return "redirect:/admin";
        } else {
            return "admin/cinema/add";
        }
    }

    @RequestMapping(value = "/admin/cinema/{id}", method = RequestMethod.PUT)
    public String update(Model model, @Valid Cinema cinema, BindingResult bindingResult) {
        if (!bindingResult.hasErrors()) {
            cinemaService.save(cinema);
            List<MovieDTO> movies = movieService.findAllByCinema(cinema.getId());
            model.addAttribute("movies", movies);
            model.addAttribute("successMessage", "Cinema has been updated successfully");
        }

        return "admin/cinema/show";
    }

    @RequestMapping(value = "/admin/cinema/{id}", method = RequestMethod.DELETE)
    public String delete(@PathVariable(value = "id") Integer id) {
        cinemaService.delete(id);

        return "admin/cinema/index";
    }

    @RequestMapping(value = "/admin/cinema/report.{format}", method = RequestMethod.GET)
    public void excelReport(@PathVariable("format") String format, HttpServletResponse response) throws IOException, DocumentException {
        cinemaReport.generate(format, response);
    }
}
