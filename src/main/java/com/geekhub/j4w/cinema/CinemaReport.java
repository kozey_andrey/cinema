package com.geekhub.j4w.cinema;

import com.geekhub.j4w.report.Report;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;

@Service
public class CinemaReport extends Report {
    private final CinemaService cinemaService;

    @Autowired
    public CinemaReport(CinemaService cinemaService) {
        this.cinemaService = cinemaService;
    }

    public void generate(String format, HttpServletResponse response) throws IOException, DocumentException {
        generate("cinema", format, response);
    }

    @Override
    public void xls(Workbook workbook) {
        Sheet sheet = workbook.createSheet();
        sheet.setDefaultColumnWidth(30);

        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setBold(true);
        style.setFont(font);

        Row header = sheet.createRow(0);
        List<String> headers = Arrays.asList("ID", "Name", "Created At", "Country", "City", "Rating");
        for (int i = 0; i < headers.size(); i++) {
            header.createCell(i).setCellValue(headers.get(i));
            header.getCell(i).setCellStyle(style);
        }

        List<CinemaDTO> cinemas = cinemaService.findAll();
        ListIterator<CinemaDTO> cinemaIterator = cinemas.listIterator();

        while (cinemaIterator.hasNext()) {
            CinemaDTO cinema = cinemaIterator.next();
            Row row = sheet.createRow(cinemaIterator.nextIndex());
            row.createCell(0).setCellValue(cinema.getId());
            row.createCell(1).setCellValue(cinema.getName());
            row.createCell(2).setCellValue(cinema.getCreatedAt());
            row.createCell(3).setCellValue(cinema.getCountry());
            row.createCell(4).setCellValue(cinema.getCity());
            row.createCell(5).setCellValue(cinema.getRating());
        }
    }

    @Override
    public PdfPTable pdf() {
        PdfPTable table = new PdfPTable(6);
        table.setWidthPercentage(100.0f);
        table.setSpacingBefore(10);

        PdfPCell cell = new PdfPCell();
        cell.setPadding(5);
        List<String> headers = Arrays.asList("ID", "Name", "Created At", "Country", "City", "Rating");
        for (String header : headers) {
            cell.setPhrase(new Phrase(header));
            table.addCell(cell);
        }

        for (CinemaDTO cinema : cinemaService.findAll()) {
            table.addCell(cinema.getId().toString());
            table.addCell(cinema.getName());
            table.addCell(cinema.getCreatedAt());
            table.addCell(cinema.getCountry());
            table.addCell(cinema.getCity());
            table.addCell(cinema.getRating().toString());
        }

        return table;
    }

    @Override
    public XWPFTable doc(XWPFDocument document) {
        XWPFTable table = document.createTable();

        XWPFTableRow tableRowOne = table.getRow(0);
        tableRowOne.getCell(0).setText("ID");
        List<String> headers = Arrays.asList("Name", "Created At", "Country", "City", "Rating");
        for (String header : headers) {
            tableRowOne.addNewTableCell().setText(header);
        }

        for (CinemaDTO cinema : cinemaService.findAll()) {
            XWPFTableRow tableRowTwo = table.createRow();
            tableRowTwo.getCell(0).setText(cinema.getId().toString());
            tableRowTwo.getCell(1).setText(cinema.getName());
            tableRowTwo.getCell(2).setText(cinema.getCreatedAt());
            tableRowTwo.getCell(3).setText(cinema.getCountry());
            tableRowTwo.getCell(4).setText(cinema.getCity());
            tableRowTwo.getCell(5).setText(cinema.getRating().toString());
        }

        return table;
    }
}
