package com.geekhub.j4w.cinema;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Repository
public interface CinemaRepository extends JpaRepository<Cinema, Integer> {
    @Query("select new com.geekhub.j4w.cinema.CinemaDTO(c.id, c.name, c.createdAt, c.country, c.city, c.rating) from Cinema c")
    @Transactional(readOnly = true)
    List<CinemaDTO> findAllByDTO();

    @Query("select new com.geekhub.j4w.cinema.CinemaDTO(c.id, c.name, c.createdAt, c.country, c.city, c.rating) from Cinema c")
    @Transactional(readOnly = true)
    Page<CinemaDTO> findAllByDTO(Pageable pageable);

    @Query("select new com.geekhub.j4w.cinema.CinemaDTO(c.id, c.name, c.createdAt, c.country, c.city, c.rating) from Cinema c inner join c.movies m where c.country = ?1 and c.city = ?2 group by c.id")
    @Transactional(readOnly = true)
    List<CinemaDTO> findAllByCountryAndCity(String country, String city);

    @Query("select new com.geekhub.j4w.cinema.CinemaDTO(c.id, c.name, c.createdAt, c.country, c.city, c.rating) from Cinema c inner join c.movies m where c.country = ?1 group by c.id")
    @Transactional(readOnly = true)
    List<CinemaDTO> findAllByCountry(String country);

    @Query("select distinct c.country from Cinema c inner join c.movies group by c.id")
    @Transactional(readOnly = true)
    List<String> findCountries();

    @Query("select distinct c.city from Cinema c inner join c.movies where c.country = ?1 group by c.id")
    @Transactional(readOnly = true)
    List<String> findCities(String country);
}
