package com.geekhub.j4w.cinema;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.geekhub.j4w.converter.LocalDateTimeConverter;
import com.geekhub.j4w.movie.Movie;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.Length;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "cinemas")
public class Cinema {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Integer id;

    @Column(name = "name")
    @Length(min = 5, max = 20, message = "*Cinema name must have at least 5 characters and be less than 20 characters")
    @NotEmpty(message = "*Please provide cinema name")
    private String name;

    @Column(name = "created_at", updatable = false, nullable = false)
    @Convert(converter = LocalDateTimeConverter.class)
    @CreationTimestamp
    private LocalDateTime createdAt;

    @Column(name = "country")
    @Length(min = 3, max = 20, message = "*Country must have at least 3 characters and be less than 20 characters")
    @NotEmpty(message = "*Please provide country of cinema")
    private String country;

    @Column(name = "city")
    @Length(min = 5, max = 20, message = "*City must have at least 5 characters and be less than 20 characters")
    @NotEmpty(message = "*Please provide city of cinema")
    private String city;

    @Column(name = "rating")
    @NotNull(message = "*Please provide cinema rating")
    private Integer rating;

    @OneToMany(mappedBy = "cinema", cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<Movie> movies = new HashSet<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Set<Movie> getMovies() {
        return movies;
    }

    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }
}
