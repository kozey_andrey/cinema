package com.geekhub.j4w.cinema;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class CinemaService {
    private final CinemaRepository cinemaRepository;

    @Autowired
    public CinemaService(CinemaRepository cinemaRepository) {
        this.cinemaRepository = cinemaRepository;
    }

    public List<CinemaDTO> findAll() {
        return cinemaRepository.findAllByDTO();
    }

    public Page<CinemaDTO> findAll(Integer page, Integer size) {
        return cinemaRepository.findAllByDTO(PageRequest.of(page, size));
    }

    public List<CinemaDTO> filteredByCountryAndCity(String country, String city) {
        return cinemaRepository.findAllByCountryAndCity(country, city);
    }

    public List<CinemaDTO> filteredByCountry(String country) {
        return cinemaRepository.findAllByCountry(country);
    }

    public Cinema findById(Integer id) {
        Optional<Cinema> cinema = cinemaRepository.findById(id);
        if (cinema.isPresent()) {
            return cinema.get();
        }

        return null;
    }

    public Cinema save(Cinema cinema) {
        return cinemaRepository.save(cinema);
    }

    public void delete(Integer id) {
        Cinema cinema = findById(id);
        cinemaRepository.delete(cinema);
    }
}
