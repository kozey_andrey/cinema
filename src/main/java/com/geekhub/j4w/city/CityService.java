package com.geekhub.j4w.city;

import com.geekhub.j4w.cinema.CinemaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class CityService {
    private final CinemaRepository cinemaRepository;

    @Autowired
    public CityService(CinemaRepository cinemaRepository) {
        this.cinemaRepository = cinemaRepository;
    }

    public List<String> findAll(String country) {
        return cinemaRepository.findCities(country);
    }
}
