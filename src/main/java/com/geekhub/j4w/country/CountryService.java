package com.geekhub.j4w.country;

import com.geekhub.j4w.cinema.CinemaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class CountryService {
    private final CinemaRepository cinemaRepository;

    @Autowired
    public CountryService(CinemaRepository cinemaRepository) {
        this.cinemaRepository = cinemaRepository;
    }

    public List<String> findAll() {
        return cinemaRepository.findCountries();
    }
}
