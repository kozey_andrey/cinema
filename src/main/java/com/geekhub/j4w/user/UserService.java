package com.geekhub.j4w.user;

public interface UserService {
    User findUserByUserName(String username);

    void saveUser(User user);
}
